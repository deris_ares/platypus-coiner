# Platypus Coiner

## What is this?
***Please note***: **this project is not production-ready yet. If you 3D-print this project with no modifications you will get a non-functional object. It will be functional soon, then this disclaimer will be removed**

Platypus Coiner is a free Euro coin holder design meant to be 3D-printed by anyone who wants to. It is coded in [OpenSCAD](https://openscad.org/). It facilitates to take coins rapidly and rip an "oh my God!" out of everyone around you, rendering you a boss

In its default form, it features coins from 1 Euro to 1 cent. 2 Euros coin was excluded for practical reasons: those are not so irritating and as a side effect, the final size of the object could be notably reduced by letting them out. Anyways, the code is there and enabling that coin (or any other) is as hard as uncommenting one line of code. It's called freedom :)

## How to use?
Start by introducing the coins in their respective holes. The height of the object allows for nine 1 Euro coins to be inserted at once, and some number greater than nine for the smaller coins. Once inside, is it recommended to put on the cover to avoid the coins to fall and prevent you from going homeless. The cover slides alongside the rails on the top of the body

The coins can be extracted from below by slightly pushing the stack up so as to align the bottom coin with the output slot. Then slide the coin outwards to get it out. The remaining stack will fall one level and you will be enabled to extract another one

## Is it really free?
In short: yes, in the terms of [GPLv3 license](https://www.gnu.org/licenses/gpl-3.0.html):

### You are enabled to:
- Download the code or [the pre-built STL objects](https://gitlab.com/deris_ares/platypus-coiner/-/tree/master/bin)
- Distribute them freely or sell them commercially
- 3D-print them
- Make modifications

### You are required to:
- Include the license and copyright notice ([LICENSE.txt](https://gitlab.com/deris_ares/platypus-coiner/-/blob/master/LICENSE.txt)) with each and every distribution
- Indicate the changes, if any, that you made to the original work
- Point to [this project's page](https://gitlab.com/deris_ares/platypus-coiner/-/tree/master) by putting a link in a clearly visible place
- Distribute, under the very same license ([GPLv3](https://www.gnu.org/licenses/gpl-3.0.html)), any modification that you make to the original work

### Keeping in mind that:
- This work is published "as is" and comes with absolutely no warranties
- I cannot be responsible in any way for any possible consequences o damages inflicted by using it
