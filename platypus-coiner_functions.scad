function alignToPrinter(value, sigma) =
    (value / sigma) % 1 == 0 ? value : sigma * floor(value / sigma + 1)
;

function atLeast(minimum, sigma) =
    minimum / sigma % 1 == 0 ? minimum : sigma * (floor(minimum / sigma) + 1)
;

function atMax(maximum, sigma) =
    maximum / sigma % 1 == 0 ? maximum : sigma * (floor(maximum / sigma))
;

function dimSum(dim, index = 0) =
    len(coins) > index + 1
        ? coins[index][dim] + dimSum(dim, index + 1)
        : coins[index][dim]
;

function dimMax(dim, index = 0, currentMax = 0) =
    len(coins) > index + 1
        ? dimMax(dim, index + 1, max(currentMax, coins[index + 1][dim]))
        : currentMax
;

function findCoin_realSize(name, index = 0) =
    len(coins) > index
        ? coins[index][0] == name
            ? coins[index]
            : findCoin_realSize(name, index + 1)
        : ["", -1, -1]
;

function pathPosition(coinIndex) =
    coins[coinIndex] == undef ? undef :
        coins[coinIndex][3] == len(coins) - 1
            ? [
                coins[coinIndex][1] / 2 + spaceBetween_XY,
                totalWidth / 2,
                spaceBetween_Z
            ]
            : [
                pathPosition(coinIndex + 1)[0] + coins[coinIndex + 1][1] / 2 + coins[coinIndex][1] / 2 + spaceBetween_XY,
                totalWidth / 2,
                spaceBetween_Z
            ]
;
