include <platypus-coiner_config.scad>

module solidBody() {
    hull() {
        translate([totalWidth / 2, totalWidth / 2, 0]) {
            cylinder(r=totalWidth / 2, h=totalHeight);
        }
        translate([totalLength - totalWidth / 2, totalWidth / 2, 0]) {
            cylinder(r=totalWidth / 2, h=totalHeight);
        }
    }
}

module makeHole(position, radius, height, coinHeight) {
    translate([position[0], position[1], position[2] - coinHeight / 2]) {
        cylinder(r=radius, h=height + coinHeight / 2);
    }
}

module makeFingerHole(railPosition, coin, coin_realSize) {
    coinDiameter = coin[1];
    coinRadius = coinDiameter / 2;
    radiusMinDifference = 0.4;
    fingerHoleRadius = atMax(coin_realSize[1] / 2 - radiusMinDifference, printerResolution_XY);
    
    translate([railPosition[0], railPosition[1], -printerResolution_Z]) {
        cylinder(r=fingerHoleRadius, h=spaceBetween_Z - coin_realSize[2] / 2 + 2 * printerResolution_Z);
    }
    
    zeroLeft = railPosition[0] - fingerHoleRadius;
    zeroFront = railPosition[1] - coinRadius;
    
    gap = spaceBetween_XY;
    
    startingLeft = zeroLeft + gap;
    startingFront = -printerResolution_XY;
    startingHeight = -printerResolution_Z;
    
    length = 2 * fingerHoleRadius - 2 * gap;
    width = railPosition[1] - startingFront;
    height = printerResolution_Z + spaceBetween_Z + printerResolution_Z;
    
    translate([startingLeft, startingFront, startingHeight]) {
        cube([length, width, height]);
    }
}

module makeRail(position, length, height) {
    translate(position) {
        cube([length, totalWidth / 2 + printerResolution_XY, height]);
    }
}

module makePath(coinIndex) {
    position = pathPosition(coinIndex);
    coin = coins[coinIndex];
    coin_realSize = coins_realSize[coinIndex];
    
    makeHole(position, coin[1] / 2, cylinderHolesHeight, coin_realSize[2]);
    makeFingerHole(position, coin, coin_realSize);
    makeRail([position[0] - coin[1] / 2, -printerResolution_XY, spaceBetween_Z], coin[1], coin[2]);
}

handlersLength = totalLength - totalWidth;

module makeHandlers() {

    difference() {
        translate([totalWidth / 2, 0, totalHeight - printerResolution_Z])
            cube([handlersLength, handlersWidth_XY, handlersWidth_Z + printerResolution_Z]);
        
        translate([totalWidth / 2 - printerResolution_XY, handlersWidth_XY / 2 - printerResolution_XY, totalHeight - 2 * printerResolution_Z])
            cube([handlersLength + 2 * printerResolution_XY, handlersWidth_XY / 2 + 2 * printerResolution_XY, handlersWidth_Z / 2 + 3 * printerResolution_Z]);
    }
    
    difference() {
        translate([totalWidth / 2, totalWidth - handlersWidth_XY, totalHeight - printerResolution_Z])
            cube([handlersLength, handlersWidth_XY, handlersWidth_Z + printerResolution_Z]);
        
        translate([totalWidth / 2 - printerResolution_XY, totalWidth - handlersWidth_XY - printerResolution_XY, totalHeight - 2 * printerResolution_Z])
            cube([handlersLength + 2 * printerResolution_XY, handlersWidth_XY / 2 + 2 * printerResolution_XY, handlersWidth_Z / 2 + 3 * printerResolution_Z]);
    }
}

module makeCoverLimit() {
    difference() {
        translate([totalLength - totalWidth / 2, totalWidth / 2, totalHeight]) {
            cylinder(r=totalWidth / 2, h=handlersWidth_Z);
        }
        translate([totalLength - totalWidth - printerResolution_XY, -printerResolution_XY, totalHeight - printerResolution_Z]) {
            cube([totalWidth / 2 + printerResolution_XY, totalWidth + 2 * printerResolution_XY, handlersWidth_Z + 2 * printerResolution_Z]);
        }
    }
}

module makeUndercover() {
    makeCoverLimit();
    
    difference() {
        translate([totalWidth / 2, handlersWidth_XY / 2 + printerResolution_XY, totalHeight]) {
            cube([totalLength - totalWidth + printerResolution_XY, totalWidth - handlersWidth_XY - 2 * printerResolution_XY, handlersWidth_Z + printerResolution_Z]);
        }
        
        translate([totalWidth / 2 - printerResolution_XY, handlersWidth_XY / 2, totalHeight + handlersWidth_Z / 2 - printerResolution_Z]) {
            cube([totalLength - totalWidth + 2 * printerResolution_XY, handlersWidth_XY / 2 + 2 * printerResolution_XY, handlersWidth_Z / 2 + 3 * printerResolution_Z]);
        }
        
        translate([totalWidth / 2 - printerResolution_XY, totalWidth - handlersWidth_XY - 2 * printerResolution_XY, totalHeight + handlersWidth_Z / 2 - printerResolution_Z]) {
            cube([totalLength - totalWidth + 2 * printerResolution_XY, handlersWidth_XY / 2 + 2 * printerResolution_XY, handlersWidth_Z / 2 + 3 * printerResolution_Z]);
        }
    }
}

module makeCover() {
    makeUndercover();
    
    hull() {
        translate([totalWidth / 2, totalWidth / 2, totalHeight + handlersWidth_Z]) {
            cylinder(r=totalWidth / 2, h=spaceBetween_Z);
        }
        translate([totalLength - totalWidth / 2, totalWidth / 2, totalHeight + handlersWidth_Z]) {
            cylinder(r=totalWidth / 2, h=spaceBetween_Z);
        }
    }
}

module makeBeltHolders() {
    difference() {
        translate([totalLength / 3 - spaceBetween_XY, totalWidth - printerResolution_XY, 0])
            cube([2 * spaceBetween_XY, 2 * spaceBetween_XY + printerResolution_XY, totalHeight]);
        
        translate([totalLength / 3 - spaceBetween_XY - printerResolution_XY, totalWidth - 2 * printerResolution_XY, -printerResolution_Z])
            cube([2 * spaceBetween_XY + 2 * printerResolution_XY, spaceBetween_XY + printerResolution_XY, totalHeight - spaceBetween_Z + printerResolution_Z]);
    }
    
    difference() {
        translate([totalLength / 3 * 2 - spaceBetween_XY, totalWidth - printerResolution_XY, 0])
            cube([spaceBetween_XY * 2, spaceBetween_XY * 2 + printerResolution_XY, totalHeight]);
        
        translate([totalLength / 3 * 2 - spaceBetween_XY - printerResolution_XY, totalWidth - 2 * printerResolution_XY, -printerResolution_Z])
            cube([spaceBetween_XY * 2 + 2 * printerResolution_XY, spaceBetween_XY + printerResolution_XY, totalHeight - spaceBetween_Z + printerResolution_Z]);
    }
}

module testCoins(coinIndex, quantity) {
    quantity = max(quantity, 1);
    for (i = [0 : quantity - 1]) {
        position = pathPosition(coinIndex);
        coin = coins_realSize[coinIndex];
        alignToRail = false;
        stackOffset = i * coin[2] + (alignToRail ? coin[2] * 0.6 : 0);
        
        translate([position[0], position[1], position[2] - coin[2] / 2 + stackOffset]) {
            color("red") cylinder(r=coin[1] / 2, h=coin[2]);
        }
    }
}
