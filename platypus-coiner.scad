include <platypus-coiner_modules.scad>

difference() {
    solidBody();
    for (index = [0 : len(coins) - 1]) makePath(index);
};

makeHandlers();
makeCover();
makeBeltHolders();
