include <platypus-coiner_functions.scad>

$fn = 64;

printerResolution_XY = 0.4;
printerResolution_Z = 0.2;

desiredSpaceBetween = 3;
spaceBetween_XY = alignToPrinter(desiredSpaceBetween, printerResolution_XY);
spaceBetween_Z = alignToPrinter(desiredSpaceBetween, printerResolution_Z);

minimumHoleSpace = 0.5;

coins_realSize = [ // name, diameter, height
    ["1c", 16.25, 1.67],
    ["2c", 18.75, 1.67],
    ["5c", 21.25, 1.67],
    ["10c", 19.75, 1.93],
    ["20c", 22.25, 2.14],
    ["50c", 24.25, 2.38],
    ["1e", 23.25, 2.33],
//  ["2e", 25.75, 2.2] // Excluded
];

coins = [for (index = [0 : len(coins_realSize) - 1]) [
    coins_realSize[index][0],
    atLeast(coins_realSize[index][1] + minimumHoleSpace, printerResolution_XY),
    atLeast(coins_realSize[index][2] + minimumHoleSpace, printerResolution_Z),
    index
]];

coinsOnStack = 9;

spacesNumberX = len(coins) + 1;
spacesNumberY = 2;
spacesNumberZ = 2;

higherCoin = findCoin_realSize("1e");
higherHoleHeight = higherCoin[2] * coinsOnStack;
cylinderHolesHeight = higherHoleHeight + spaceBetween_Z + printerResolution_Z;

totalLength = dimSum(1) + spaceBetween_XY * spacesNumberX;
totalWidth = dimMax(1) + spaceBetween_XY * 2;
totalHeight = higherHoleHeight + spaceBetween_Z * spacesNumberZ;

desiredHandlersWidth = 4;

handlersWidth_XY = alignToPrinter(desiredHandlersWidth, printerResolution_XY);
handlersWidth_Z = alignToPrinter(desiredHandlersWidth, printerResolution_Z);
